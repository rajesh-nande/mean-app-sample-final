(function() {

    function getFeeds(fee_d, options, helpers, cb) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Feeds = dbClient.collection('feeds');
                Feeds.find(fee_d).toArray(function(retrievalErr, feeds) {
                    if (!retrievalErr) {
                        helpers.execute(cb, [feeds]);
                    } else {
                        helpers.execute(retrievalErr, null);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    function createFeeds(fee_d, options, helpers, cb, result, brand_id, email) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Feeds = dbClient.collection('feeds');
                Feeds.find({brand_id: fee_d.brand_id, email:fee_d.email}).toArray().then((result) => {
                    console.log('result', result.length)
                    if (result.length !== 0) {
                        helpers.execute(cb, ['already rating']);
                    } else {
                        Feeds.insertOne(fee_d, function(retrievalErr, feeds) {
                            if (!retrievalErr) {
                                helpers.execute(cb, [feeds]);
                            } else {
                                helpers.execute(retrievalErr, null);
                            }
                        });
                    }
                });
            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    exports.Feeds = {
        getFeeds: getFeeds,
        createFeeds: createFeeds
    }

})();