(function() {


    function execute(req, res, next, helpers) {
        var feed = require('../data_access/feeds').Feeds;
        var manager = require('../data_access/managers').Managers;

        var Bid = req.params.id
        var Access = req.params.access
        if (Access == 'true') {
            Access = true
            manager.getManagersList({ "brand_id": Bid, "has_access": Access }, {}, helpers, function(managers) {

                if (managers == "") {

                    res.send("Alert: Not found Manager of this Brand..\n---------------------------------------")
                } else {

                    feed.getFeeds({ "brand_id": Bid }, {}, helpers, function(feeds) {
                        if (feeds == "") {
                            res.send("Alert : feeds not found of this Brand..\n---------------------------------------")

                        } else {

                            res.send(feeds)
                        }
                    })
                }
            })
        } else if (Access == 'false') {
            Access = false

            manager.getManagersList({ "brand_id": Bid, "has_access": Access }, {}, helpers, function(managers) {

                if (managers == "") {

                    res.send("Alert: Not found Manager of this Brand..\n---------------------------------------")
                }else{

                	res.send({No_access_Permission:managers})
                }
            })
        }





    }

    exports.execute = execute
})()